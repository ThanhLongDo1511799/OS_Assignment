#include <procmem.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>


int main(){
	pid_t mypid= getpid();
	printf("PID: %d\n",mypid);
	struct proc_segs info;
	
	if(procmem(mypid,&info)==0){
		printf("student ID: %lu\n",info.mssv);
		printf("code segment: %lx - %lx\n",info.start_code, info.end_code);
		printf("data segment: %lx - %lx\n",info.start_data,info.end_data);
		printf("heap segment: %lx - %lx \n",info.start_heap, info.end_heap);
		printf("start stack: %lx\n", info.start_stack);
	}
	else{
		printf("cant not get information about process\n");
	}
	sleep(100);
	return 0;
}
