#include "procmem.h"
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <unistd.h>

#define SIZE 10
#define SYSCALL_ID 546

long procmem(pid_t pid, struct proc_segs * info){
	long sysvalue;
	unsigned long buff[SIZE];
	sysvalue=syscall(SYSCALL_ID,pid,buff);
	
	info->mssv=buff[0];
	info->start_code=buff[1];
	info->end_code=buff[2];
	info->start_data=buff[3];
	info->end_data=buff[4];
	info->start_heap=buff[5];
	info->end_heap=buff[6];
	info->start_stack=buff[7];
	
	return 0;
}

//function to test
/*
int main(){
	struct proc_segs * info =malloc(sizeof(struct proc_segs));
	if(info!= NULL)	{
		printf("!NULL");
		procmem(1,info);
		printf("my MSSV: %lu \n",info->mssv);
		printf("start_code - end_code: %lx -%lx \n",info->start_code,info->end_code);
		printf("start_data - end_data: %lx -%lx \n",info->start_data,info->end_data);
		printf("start_heap - end_heap: %lx -%lx \n",info->start_heap,info->end_heap);
		printf("start_stack: %lx\n",info->start_stack);
	}

	return 0;
}*/
